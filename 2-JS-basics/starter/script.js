/**********************************
 * Variables and Datatypes
 */

/* var firstname = 'Ronnie';
console.log(firstname);

var lastname = 'Gibbons';
var age = 21;

var fullAge = true;
console.log(fullAge);

var job;
console.log(job);

job = 'Mechanic';
console.log(job);
*/

/*****************************************
 * Variable Mutation
 */

/*
alert(firstname + ' is a ' + age + ' year old.');

prompt('What is your name?');
*/


/**********************************
 *  Basic Operators
 */
/*
var year, yearRonnie;
    year = 2020
    
    //Math operators
    yearRonnie = year - 21;
    yearFred = year - 24;

    console.log(year + 2);
    console.log(year * 2);
    console.log(year / 2);

    //Logical operators
    var isOlder = yearRonnie < yearFred;
    console.log(isOlder);

    //Typeof operator
    console.log(typeof isOlder);
    console.log(typeof yearRonnie);
    console.log(typeof 'HI');
    var x;
    console.log(typeof x);
*/

/*************************************
 * Coding challenge 1
 */

 /*
 var johnMass, johnHeight, markMass, markHeight;
 johnMass = 95; //kg
 johnHeight = 1.8; //meters
 markMass = 80; //kg
 markHeight = 1.6; //meters

 johnBmi = johnMass / (johnHeight * johnHeight);
 markBmi = markMass / (markHeight * markHeight);

 var bmiIsHigher = markBmi > johnBmi == true; 

 console.log('Is Mark\'s BMI higher than John\'s?' + bmiIsHigher);
*/

/***********************************
 * If / else statements
 */
/*
 var firstName = 'Ronnie';
 var civilStatus = 'single';

 if (civilStatus === 'married') {
     console.log(firstName + ' is married! ');
 }  else {
     console.log(firstName + ' will hopefully marry soon!');
 }
    
 var isMarried = true;

 if (isMarried) {
    console.log(firstName + ' is married! ');
}  else {
    console.log(firstName + ' will hopefully marry soon!');
}
*/

/*********************************
 * Boolean logic
 */

 var firstName = 'Ronnie';
 var age = 21;

 if (age < 13) {
    console.log(firstName + ' is a boy. ');
 }  else if 
    console.log(firstName + ' is a teenager. ');
 
    else {
     console.log(firstName + ' is a man. ');
 };